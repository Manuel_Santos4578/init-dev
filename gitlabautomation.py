import requests
from dotenv import load_dotenv
import os


def create_gitlab_repo(name: str):
    url = 'https://gitlab.com/api/v4/projects?name=' + name
    load_dotenv()
    API_TOKEN = os.getenv('API_TOKEN')
    response = requests.post(url, headers={'Authorization': 'Bearer ' + API_TOKEN})
    content = response.json()
    http_url_to_repo = content['http_url_to_repo']
    os.system("git remote add origin "+http_url_to_repo)

if __name__ == '__main__':
    exit()
