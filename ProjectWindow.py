import os
import sys
import tkinter
import tkinter.constants
import tkinter.filedialog
import json
import time
import re
import gitlabautomation

class ProjectWindow:
    project_path:str

    def __init__(self, project_path):
        self.window = tkinter.Tk()
        self.window.title('Init Dev')
        self.window.geometry('600x300')
        self.window.configure(bg='#75f9e1')
        self.window.resizable(False, False)
        self.project_path = project_path
        self.init_content()

    def show(self):
        self.window.mainloop()

    def init_content(self):
        self.project_name_label_init()
        self.project_name_entry_init()
        self.project_types_label_init()
        self.project_list_init()
        self.status_label_init()
        self.create_button_init()
        self.refresh_list_button_init()
        self.push_remote_label_init()
        self.push_remote_drop_init()

    def fill_listbox(self):
        self.projects = self.get_projects()
        self.project_list.delete(0, tkinter.END)
        for type_of_project in self.projects['projects']:
            self.project_list.insert(tkinter.END, type_of_project['name'])

    def create_action(self):
        if len(self.project_list.curselection()) != 0:
            self.project_type_picked = self.project_list.get(
                self.project_list.curselection())
        project = self.project_name_entry.get()
        self.folder_path = tkinter.filedialog.askdirectory()
        if self.folder_path:
            remote = self.default.get()
            self.create_project(self.project_type_picked,self.folder_path,project,remote)
        pass

    def get_projects(self):
        file = open(self.project_path+os.path.sep + 'projects.json', 'r')
        data = file.read()
        return json.loads(data)

    # region Components
    def project_name_label_init(self):
        self.project_name_label = tkinter.Label(
            self.window, text="Nome do projeto", bg='#53aa9a', fg='white')
        self.project_name_label.place(x=199, y=10)

    def project_name_entry_init(self):
        self.project_name_entry = tkinter.Entry(self.window, width=30)
        self.project_name_entry.place(x=300, y=10)

    def project_types_label_init(self):
        self.project_types_label = tkinter.Label(
            self.window, text="Tipos de projeto", bg='#53aa9a', fg='white')
        self.project_types_label.place(x=15, y=10)

    def project_list_init(self):
        self.project_list = tkinter.Listbox(
            self.window, selectmode=tkinter.SINGLE)
        self.fill_listbox()
        self.project_list.place(x=15, y=30)

    def status_label_init(self):
        self.status_label = tkinter.Label(self.window, text=None, bg='#75f9e1')
        self.status_label.place(x=200, y=100)

    def create_button_init(self):
        self.create_button = tkinter.Button(
            self.window, show=None, text='criar', width=10, bg='#53aa9a', fg='white', command=self.create_action)
        self.create_button.place(x=300, y=150)

    def refresh_list_button_init(self):
        self.refresh_list_button = tkinter.Button(
            self.window, show=None, text='atualizar lista', bg='#53aa9a', fg='white', width=15, command=self.fill_listbox)
        self.refresh_list_button.place(x=15, y=200)

    def push_remote_label_init(self):
        self.push_remote_label = tkinter.Label(self.window,text='Criar repositório', bg='#53aa9a', fg='white',height=1)
        self.push_remote_label.place(x=199,y=50)

    def push_remote_drop_init(self):
        options = ['Sim','Não', ]
        self.default = tkinter.StringVar()
        self.default.set(options[0])
        self.push_remote_drop = tkinter.OptionMenu(
            self.window, self.default, *options)
        self.push_remote_drop.config(
            bg='#53aa9a', fg='white', width=10, height=1,direction='right')
        self.push_remote_drop['menu'].config(bg='#53aa9a', fg='white')
        self.push_remote_drop.place(x=300, y=50)

    # endregion

    def create_project(self, project_type, folder_path, name, push_remote):
        try:
            projects = self.get_projects()
            os.mkdir(folder_path + os.path.sep + name)
            os.chdir(folder_path + os.path.sep + name)
            self.status_label.config(
                text="Initializing git repo", bg='#53aa9a', fg='white')
            time.sleep(1)
            os.system('git init')
            os.system(f'echo "# {name}" > README.md')
            os.system(f'echo "" > .gitignore')

            if project_type != '' and project_type != '---':
                self.status_label.config(text="Creating project")
                time.sleep(1)
                for type_of_project in projects['projects']:
                    if type_of_project['name'].lower() == project_type:
                        for command in type_of_project['commands']:
                            if '@project_name' in command:
                                command = re.sub(
                                    r'@project_name', name, command)
                                pass
                            os.system(command)
                            pass
                        pass
                    pass

            self.status_label.config(text="Commiting changes")
            time.sleep(1)
            os.system('git add .')
            os.system('git commit -m "Initial commit"')

            self.status_label.config(text=f'{name} created successfully')
            if push_remote == 'Sim':
                time.sleep(1)
                self.status_label.config(text='A criar repositório gitlab')
                gitlabautomation.create_gitlab_repo(name)
                os.system("git push -u origin --all")
            time.sleep(1)
            os.system('code .')
        except Exception as ex:
            self.status_label.config(text=ex)
        pass

if __name__ == '__main__':
    exit()
